const users = require('./users.json');

//
// Obtener el usuario cuyo teléfono es "024-648-3804"
//
console.log(`\n[1]-Ejercicio: usuario cuyo teléfono es '024-648-3804'`);
console.log(users.find(u => u.phone === '024-648-3804'));

//
// Crear una función que devuelva true si existe un usuario cuyo email
// sea el que se pasa como parámetro
//
console.log(`\n[2]-Ejercicio: devolver true si existe un usuario cuyo email sea el que se pasa como parámetro`);

function existsUser(email){
    return users.some(u => u.email === email);
}

console.log(existsUser('Nathan@yesenia.net')); // true
console.log(existsUser('japostigo@atsistemas.com')); // false

//
// Obtener el número de usuarios que tienen website
//
console.log(`\n[3]-Ejercicio: Obtener el número de usuarios que tienen website:`);
console.log(users.filter(u => u.website !== undefined).length);


//
// Obtener el índice de la posición que toma en el array el primer usuario
// cuyo número de la calle de su dirección es menor que 300
//
console.log(`\n[4]-Ejercicio: Obtener el índice del primer usuario cuyo número de la calle de su dirección es menor que 300`);
console.log(users.findIndex(u => u.address.number < 300));


//
// Obtener un array que sólo contenga las cadenas de los emails de los usuarios
//
console.log(`\n[5]-Ejercicio: Obtener un array que sólo contenga las cadenas de los emails de los usuarios`);
console.log(users.map(u => u.email));


//
// Obtener un array que contengan objetos {id: "id", username: "username"},
// que contienen los ids y los nombres de usuarios de los usuarios
//
console.log(`\n[6]-Ejercicio: Obtener un array que sólo contenga los ids y los nombres de usuarios de los usuarios`);
console.log(users.map(u => ({id: u.id, username: u.username})));


//
// Obtener el array de usuarios pero con los números de sus direcciones en
// formato de número (y no de cadena que es como está ahora mismo)
//
console.log(`\n[7]-Ejercicio: Obtener un array de usuarios pero con los números de sus direcciones en formato de número`);
console.log(users.map ( u => {
    if(u.address != null){
        u.address.number = +u.address.number
    }
    return u;
}));


//
// Obtener el array de usuarios cuya dirección está ubicada entre la
// latitud -50 y 50, y la longitud -100 y 100
//
console.log(`\n[8]-Ejercicio: Obtener un array de usuarios cuya dirección está ubicada entre la latitud -50 y 50, y la longitud -100 y 100`);
console.log(users.filter(u => u.address?.geo.lng >= -100 && u.address?.geo.lng <= 100
    && u.address?.geo.lat >= -50 && u.address?.geo.lat <= 50)
);



//
// Obtener un array con los teléfonos de los usuarios cuyo website
// pertenezca a un dominio biz
//

console.log(`\n[9]-Ejercicio: Obtener un array con los teléfonos de los usuarios cuyo website pertenezca a un dominio biz`);
console.log(users.filter(u => u.website?.includes('biz'))
.map(u => u.phone));



//
// Escriba una función processArray que, dado un array de números
// enteros, devuelva un nuevo array en que aquellos elementos que
// sean pares se multipliquen por 2.
//
console.log(`\n[10]-Ejercicio: Función 'processArray' `);

const testArray = [2, 3, 5, 6, 5, 9, 10, 12, 13];

const processArray = (arr) => arr.map(a => {
    if(a % 2 == 0){
        return a * 2;
    }else{
        return a;
    }
});
console.log(processArray(testArray)); // [4, 12, 20, 24]



//
// Dado un array de cadenas, obtenga un objeto que tenga como claves cada
// una de las cadenas y cada uno de los valores de esas claves sea false
//

console.log(`\n[11]-Ejercicio: Array cadenas y claves`);

const keys = ['key1', 'key2', 'key3'];
const map = new Map();
keys.map( k => map.set(k , false));
console.log(map);

// Result: { key1: false, key2: false, key3: false }



//
// Obtenga un objeto de la agrupación de los nombres de usuarios por nombre de empresa.
// Las claves del objeto serán los nombres de empresa y los valores de cada
// clave un array con los nombres de pila de los usuarios que pertenecen a esa
// empresa.
//
console.log(`\n[12]-Ejercicio: agrupación de los nombres de usuarios por nombre de empresa`);
const findUsersByUniqueCompanyName = () => {
     const uniqueCompanies = new Set(users.map( us => (us.company.name)))
     const companyMap = new Map();
     uniqueCompanies.forEach(company => { 
         let userFilterArray = users.filter(u => company === u.company.name);
         companyMap.set(company, userFilterArray.map(u => u.name));
     });
     return companyMap;
}
console.log(findUsersByUniqueCompanyName());

// {
//   'Romaguera-Crona': [ 'Leanne Graham', 'Clementine Bauch' ],
//   'Deckow-Crist': [ 'Ervin Howell', 'Glenna Reichert' ],
//   'Robel-Corkery': [ 'Patricia Lebsack', 'Mrs. Dennis Schulist' ],
//   'Keebler LLC': [ 'Chelsey Dietrich' ],
//   'Hoeger LLC': [ 'Kurtis Weissnat', 'Clementina DuBuque' ],
//   'Abernathy Group': [ 'Nicholas Runolfsdottir V' ]
// }
