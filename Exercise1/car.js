class Car{
    constructor(brand) {
        this.brand = brand;
        this.km = 0;
    }

    move(km){
        this.km += km;
    }

    toString() {
        return `Car-Data: This ${this.brand} has ${this.km}Km.`
    }
}

module.exports = Car;