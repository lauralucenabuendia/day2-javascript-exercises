//
// Crea un modulo `car.js` que contenga una clase `Car` con dos atributos:
// `brand` y `km`.
//
// El constructor sólo aceptaría el parámetro `brand`, porque `km` se inicia a 0.
//
// Dispondría de dos métodos:
//  - `move(km)`: incrementa `km` según la cantidad
//  - `toString()`: que devuelve un string con `brand` y `km`
//
// Importa el módulo en este archivo para instanciar un coche `wv` de la marca
// 'Volkswagen', regístrale un movimiento de 100 km y luego mostrar por consola
// la salida de su método `toString()`
console.log('1 EXERCISE')
const Car = require('./car.js')

let car = new Car('Volkswagen');
car.move(100);
console.log(car.toString());


// //
// // Refactoriza el siguiente código usando la sintaxis de ES6
// // y la función iteradora correcta
// //
console.log('\n2 EXERCISE')

const PI = 3.14;
const radius = [2, 3, 5];

const getCircleArea = (r) => PI * r * r;

radius.forEach(r => console.log(`\nRadius = ${r}\nArea= ${getCircleArea(r)}`));


// //
// // TODO 3
// //
// // Refactoriza el siguiente código usando la sintaxis de ES6
// //
console.log('\n3 EXERCISE')

let prop2 = 'value2';

var obj = {
  prop1: 'value1',
  prop2,

  function1(param = 1) {
    // console.log(param);
    return param;
  }
};

//Testeando
console.log(obj);
console.log(obj.function1());
console.log(obj.function1(88));


// //
// //
// // Refactoriza el siguiente código usando la sintaxis de ES6
// //
console.log('\n4 EXERCISE')

const object = {
  a: 1,
  b: 2,
  c: 3,
  d: 4,
  e: 5
};

const { a, b, c, d, e } = object;

//Testeando
console.log(`Variables:`);
console.log(a);
console.log(c);
console.log(e);


const array = [0, 1, 2, 3, 4, 5];
const [, a1, , a3, a4, a5] = array;

//Testeando
console.log(`Array:`)
console.log(a1);
console.log(a3);
console.log(a4);
console.log(a5);


// //
// //
// // Refactoriza el siguiente código usando la sintaxis de ES6
// //
console.log('\n5 EXERCISE')

const array1 = [0, 1, 2, 3];
const array2 = [4, 5, 7, 8];

const res = [...array1, ...array2];

console.log(res);


// //
// // Crea un nuevo objeto `discountedFridge`, basado en el objeto `fridge` que se
// // provee, conservando las mismas propiedades, pero que tenga una nueva propiedad
// // `isDiscounted` a `true` y un 20% de descuento aplicado a su precio `price`
// //
console.log('\n6 EXERCISE')

var fridge = {
  id: 1,
  name: 'Fridge',
  description: 'Amazing fridge',
  price: 500
};

//--
const discountedFridge = {
    ...fridge,
    isDiscounted: true,
    price: fridge.price - ( fridge.price * 0.2)
  };
  console.log(discountedFridge)

// //
// //
// // Implementa una función `mergeFiles(pathSrc1, pathSrc2, pathFileOut)` que lea
// // el contenido de los ficheros cuyas rutas son `pathSrc1` y `pathSrc1` y
// // escriba en el fichero con ruta `pathFileOut` el resultado de concatenar
// // ambos contenidos. Que muestre por consola 'Output file wrote!' **solo si ha
// // ido bien**. En caso de error, que muestre la información del error por consola.
// //
// // Invócala con los ficheros de entrada `./input1.txt` y './input2.txt' y
// // y escribe el resultado en `./output.txt`.
// //
// // fs.writeFile(filePath, fileContents)
// //

const fs = require('fs/promises');

async function mergeFiles (pathSrc1, pathSrc2, pathFileOut) {
  try {
    let fileData = await fs.readFile(pathSrc1, 'utf8');
    fileData += await fs.readFile(pathSrc2, 'utf8');
    
    fs.writeFile( pathFileOut, fileData);
    console.log('Output file wrote!');

  } catch(error) {
    console.error(error);
  }
}

mergeFiles('./input1.txt', './input2.txt', './output.txt');